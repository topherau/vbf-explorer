﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VBFTool.zlib.net;

namespace VBFTool.VBF
{
    public class VbfUtility
    {
        /// <summary>
        /// Read and compress bytes from the <paramref name="input"/> stream, writing the
        /// compressed bytes to the <paramref name="output"/> stream. This overload will
        /// read from the current position to the end of the input stream.
        /// </summary>
        /// <param name="input">The stream to read the uncompressed bytes from.</param>
        /// <param name="output">The stream to write the compressed bytes to.</param>
        /// /// <returns>A list that contains the length of each compressed block.</returns>
        public List<ushort> Compress(Stream input, Stream output) =>
            Compress(input, output, input.Length - input.Position);

        /// <summary>
        /// Read and compress the specified number of bytes from the <paramref name="input"/>
        /// stream, writing the compressed bytes to the <paramref name="output"/> stream.
        /// </summary>
        /// <param name="input">The stream to read the uncompressed bytes from.</param>
        /// <param name="output">The stream to write the compressed bytes to.</param>
        /// <param name="count">The number of uncompressed bytes to read from the input stream.</param>
        /// <returns>A list that contains the length of each compressed block.</returns>
        public static List<ushort> Compress(Stream input, Stream output, long count)
        {
            var blockList = new List<ushort>();
            var bytesRemaining = count;
            while (bytesRemaining > 0)
            {
                // Block size is 64k or remaining bytes, whichever is lower
                var bytesToRead = bytesRemaining > 65536 ? 65536 : (int) bytesRemaining;

                // Read uncompresssed bytes into buffer
                var inBuffer = new byte[bytesToRead];
                var bytesRead = input.Read(inBuffer, 0, bytesToRead);
                bytesRemaining -= bytesRead;

                using (var deflated = new MemoryStream()) //            Stream for deflated bytes
                using (var deflater = new ZOutputStream(deflated, //    Deflater that writes to stream
                    zlibConst.Z_BEST_COMPRESSION))
                {
                    // Write the decompressed buffer into the deflater and finalize the compression
                    deflater.Write(inBuffer, 0, bytesRead);
                    deflater.finish();

                    // Check if compression result was viable
                    if (deflated.Length >= bytesRead)
                    {
                        // Non-viable, write decompressed bytes to stream
                        blockList.Add((ushort) (bytesRead == 65536 ? 0 : bytesRead));
                        output.Write(inBuffer, 0, bytesRead);
                    }
                    else
                    {
                        // Viable, write compressed bytes to stream
                        blockList.Add((ushort) deflated.Length);
                        deflated.WriteTo(output);
                    }
                }
            }

            return blockList;
        }

        public static void Decompress(Stream input, Stream output, long originalSize, IEnumerable<ushort> blockList, int blockStart)
        {
            var blockCount = (int) (originalSize / 65536 + (originalSize % 65536 == 0 ? 0 : 1));
            var blocks = blockList.ToList().GetRange(blockStart, blockCount).ToList();
            Decompress(input, output, originalSize, blocks);
        }

        public static void Decompress(Stream input, Stream output, long originalSize, IEnumerable<ushort> blockList)
        {
            // Block remainder is decompressed size of last block, or 64k if decompressed
            var blockRemainder = (int) (originalSize % 65536);
            if (blockRemainder == 0)
                blockRemainder = 65536;

            // Iterate block list and decompress from input stream
            var blocks = blockList.ToList();
            for (var i = 0; i < blocks.Count; i++)
            {
                // Get block length as integer
                var blockLength = (int)blocks[i];

                // Since it's usually a 16 bit value, 65536 is stored as 0
                if (blockLength == 0)
                    blockLength = 65536;

                // Read compressed block from input stream
                var compressedBytes = new byte[blockLength];
                var bytesRead = input.Read(compressedBytes, 0, blockLength);

                // The decompressed length is always 64k except for the last block
                var decompressedLength = i != blocks.Count - 1
                    ? 65536
                    : blockRemainder;

                byte[] decompressedBytes;

                // Last block is uncompressed if size equals block remainder
                if (i == blocks.Count - 1)
                    if (blockLength == blockRemainder)
                        goto UncompressedBlock;

                // Uncompressed blocks have 64k length
                if (blockLength != 65536)
                {
                    // Block is compressed, attempt to decompress block
                    try
                    {
                        // Create decompressed buffer
                        decompressedBytes = new byte[decompressedLength];

                        // Write compressed bytes to stream and decompress
                        using (var inflater = new ZOutputStream(new MemoryStream(decompressedBytes)))
                            inflater.Write(compressedBytes, 0, bytesRead);

                        goto WriteBuffer;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("D'oh!", ex);
                    }
                }

                // Uncompressed block, use compressed buffer as decompressed
                UncompressedBlock:
                decompressedBytes = compressedBytes;

                // Write buffer to output stream
                WriteBuffer:
                output.Write(decompressedBytes, 0, decompressedLength);
            }
        }
    }
}