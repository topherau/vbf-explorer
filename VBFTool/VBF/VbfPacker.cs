﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using VBFTool.zlib.net;

namespace VBFTool.VBF
{
    public class VbfPacker
    {
        private static MD5 _md5 = MD5.Create();

        private readonly string _filename;
        private List<VbfEntry> _entries;

        public VbfPacker(string filename)
        {
            _filename = filename;
            _entries = new List<VbfEntry>();
        }

        public void Add(string sourceFile, string pathInArchive)
        {
            var normalizedPath = NormalizePath(pathInArchive);
            var fileInfo = new FileInfo(sourceFile);

            _entries.Add(new VbfEntry
            {
                Source = sourceFile,
                Path = normalizedPath,
                Hash = ComputeHash(normalizedPath),
                OriginalSize = fileInfo.Length
            });
        }

        public void Pack()
        {
            var numFiles = _entries.Count;
            Console.WriteLine($"Packing {numFiles} files...");

            // Create output stream for VBF
            var outStream = File.Create(_filename);
            var outWriter = new BinaryWriter(outStream);

            // Write initial header data
            outWriter.Write((uint) 1264144979); //   Header identifier
            var posHeaderLength = outStream.Position;
            outWriter.Write((uint) 0); //            Header length, written afterwards
            outWriter.Write((ulong) numFiles); //    Number of files in archive

            // Generate the string table and write hashes to output stream
            var fileNameOffsets = new ulong[numFiles];
            long posDataTable; //                    Data table position
            using (var tableStream = new MemoryStream())
            {
                for (var i = 0; i < numFiles; i++)
                {
                    // Convert path to ASCII byte array
                    var stringBytes = Encoding.ASCII.GetBytes(_entries[i].Path);

                    // Store string table position in offset array
                    fileNameOffsets[i] = (ulong) tableStream.Position;

                    // Write path as null-terminated string to string table
                    tableStream.Write(stringBytes, 0, stringBytes.Length);
                    tableStream.WriteByte(0);

                    // Write MD5 hash to output stream
                    outStream.Write(_entries[i].Hash, 0, 16);
                }

                // Store current offset for data table and seek ahead to string table
                posDataTable = outStream.Position;
                outStream.Seek(numFiles * 32, SeekOrigin.Current);

                // Write string table length and bytes to output stream
                var tableBytes = tableStream.ToArray();
                outWriter.Write((uint) tableBytes.Length + 4U);
                outStream.Write(tableBytes, 0, tableBytes.Length);
            }

            // Calculate total number of blocks in archive
            var totalBlocks = _entries.Sum(e => e.BlockCount);

            // Store block list position and seek ahead to compressed data block
            var posBlockList = outStream.Position;
            outStream.Seek(totalBlocks * 2, SeekOrigin.Current); //     2 bytes for each block length
            var headerLength = (int)outStream.Position; //              Header ends after the block list

            // Pack the data and generate the block list
            var startOffsets = new ulong[numFiles]; //      Offsets of each file in archive
            var blockList = new ushort[totalBlocks]; //     List of compressed block lengths
            var blockListStarts = new uint[numFiles]; //    Start offset of file in block list

            var currentBlock = 0U;
            for (var i = 0; i < _entries.Count; i++)
            {
                using (var inStream = File.Open(_entries[i].Source, FileMode.Open))
                {
                    startOffsets[i] = (ulong) outStream.Position;
                    blockListStarts[i] = currentBlock;

                    // Read blocks until end of file
                    while (inStream.Position < inStream.Length)
                    {
                        // Read block from source file
                        var inBuffer = new byte[65536];
                        var bytesRead = inStream.Read(inBuffer, 0, 65536);

                        using (var deflated = new MemoryStream())
                        using (var deflater = new ZOutputStream(deflated, zlibConst.Z_BEST_COMPRESSION))
                        {
                            // Deflate block into memory stream
                            deflater.Write(inBuffer, 0, bytesRead);
                            deflater.finish();

                            // Check if compression was viable
                            if (deflated.Length >= bytesRead)
                            {
                                // Compression not viable, write uncompressed block
                                blockList[currentBlock++] = (ushort) (bytesRead == 65536 ? 0 : bytesRead) ;
                                outStream.Write(inBuffer, 0, bytesRead);
                            }
                            else
                            {
                                // Compression was viable, write compressed block
                                blockList[currentBlock++] = (ushort) deflated.Length;
                                deflated.WriteTo(outStream);
                            }
                        }
                    }
                }
            }

            // Write the header length at the stored offset
            outStream.Seek(posHeaderLength, SeekOrigin.Begin);
            outWriter.Write(headerLength);

            // Write the data table at the stored offset
            outStream.Seek(posDataTable, SeekOrigin.Begin);
            for (var i = 0; i < numFiles; i++)
            {
                outWriter.Write(blockListStarts[i]);
                outWriter.Write(0);
                outWriter.Write((ulong) _entries[i].OriginalSize);
                outWriter.Write(startOffsets[i]);
                outWriter.Write(fileNameOffsets[i]);
            }

            // Write the block list at the stored offset
            outStream.Seek(posBlockList, SeekOrigin.Begin);
            foreach (var block in blockList)
                outWriter.Write(block);

            // Read full header into buffer
            var headerBuffer = new byte[headerLength];
            outStream.Seek(0, SeekOrigin.Begin);
            outStream.Read(headerBuffer, 0, headerLength);

            // Compute hash of header and write to end of file
            var hashBuffer = _md5.ComputeHash(headerBuffer, 0, headerLength);
            outStream.Seek(0, SeekOrigin.End);
            outStream.Write(hashBuffer, 0, hashBuffer.Length);

            outStream.Close();
        }

        public void PackDirectory(string directory)
        {
            var files = Directory.GetFiles(directory, "*", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                var filePath = file.Substring(directory.Length);
                Add(file, filePath);
            }

            Pack();
        }

        private byte[] ComputeHash(string path)
        {
            var bytes = Encoding.ASCII.GetBytes(path);
            return _md5.ComputeHash(bytes);
        }

        private string NormalizePath(string path)
        {
            return path.Replace("\\", "/").TrimStart('/');
        }
    }

    public class VbfEntry
    {
        public string Source { get; set; }
        public string Path { get; set; }
        public byte[] Hash { get; set; }
        public long OriginalSize { get; set; }
        public int BlockCount => (int)(OriginalSize / 65536) + (OriginalSize % 65536 != 0 ? 1 : 0);
    }
}