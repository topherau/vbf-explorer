﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using VBFTool.VBF;

namespace VBFTool
{
    class Program
    {
        private static readonly string ExeName = Path.GetFileName(Assembly.GetExecutingAssembly().Location);
        private static string _source = null;
        private static string _target = null;
        private static int _mode;

        static void Main(string[] args)
        {
            Console.WriteLine($"VBFTool v1.0 by Topher\n---");
            if (!ProcessArgs(args))
            {
                PrintHelp();
                return;
            }

            switch (_mode)
            {
                case 0:
                    Unpack();
                    break;
                case 1:
                    Pack();
                    break;
            }

            Console.Read();
        }

        private static void Unpack()
        {
            if (!File.Exists(_source))
            {
                Console.WriteLine($"Error unpacking \"{_source}\": File not found.");
                return;
            }

            if (_target == null)
            {
                var filename = Path.GetFileNameWithoutExtension(_source);
                _target = $"{filename}_VBF";
            }

            var v = new VbfReader(_source);
            foreach (var file in v.FileList)
            {
                try
                {
                    var dir = Path.GetDirectoryName(file);
                    if (dir == null)
                        continue;

                    var path = Path.Combine(_target, dir);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    Console.WriteLine($"Extracting {file}");
                    using (var fs = File.Create(Path.Combine(_target, file)))
                        v.ExtractToStream(file, fs);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Failed to extract \"{file}\": {ex.Message}");
                }
            }
        }

        private static void Pack()
        {
            if (!Directory.Exists(_source))
            {
                Console.WriteLine($"Error packing \"{_source}\": File not found.");
                return;
            }

            if (_target == null)
            {
                _target = $"{_source}_VBF.vbf";
            }

            try
            {
                var v = new VbfPacker(_target);
                v.PackDirectory(_source);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An exception occurred during packing:\n{ex.Message}");
            }
           
        }

        static bool ProcessArgs(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("No arguments specified.");
                return false;
            }

            switch (args[0])
            {
                case "-u": // accept 1 or 2 parameters, source and optionally target
                case "--unpack":
                    if (args.Length < 2)
                    {
                        Console.WriteLine(
                            $"No source file specified.\nUsage:\n  {ExeName} -u/--unpack <source> [target]");
                        return false;
                    }

                    _source = args[1];
                    if (args.Length > 2)
                        _target = args[2];
                    _mode = 0;
                    return true;
                case "-p":
                case "--pack":
                    if (args.Length < 2)
                    {
                        Console.WriteLine(
                            $"No source directory specified.\nUsage:\n  {ExeName} -p/--pack <source> [target]");
                        return false;
                    }

                    _source = args[1];
                    if (args.Length > 2)
                        _target = args[2];
                    _mode = 1;
                    return true;
                case "-x":
                case "--extract":
                    _mode = 2;
                    return true;
                default:
                    return false;
            }
        }

        static void PrintHelp()
        {
            Console.WriteLine("YA DUN GOOFED");
        }
    }
}