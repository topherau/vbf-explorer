﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using VBFExplorer.zlib.net;

namespace VBFExplorer.VBF
{
    public class VbfMetadata
    {
        private static MD5 _md5 = MD5.Create();

        public DateTime LastModified { get; set; } = DateTime.Now;
        public List<VbfMetadataEntry> OriginalFiles { get; set; } = new List<VbfMetadataEntry>();

        public static byte[] Compress(VbfMetadata metadata)
        {
            var json = JsonConvert.SerializeObject(metadata);
            var bytes = Encoding.UTF8.GetBytes(json);

            var deflated = new MemoryStream();
            var deflater = new ZOutputStream(deflated, zlibConst.Z_BEST_COMPRESSION);

            deflater.Write(bytes, 0, bytes.Length);
            deflater.finish();

            var deflatedBytes = deflated.ToArray();
            var deflatedHash = _md5.ComputeHash(deflatedBytes);

            var output = new MemoryStream();
            output.Write(BitConverter.GetBytes((int) deflated.Length), 0, 4);
            output.Write(deflatedHash, 0, 16);
            output.Write(deflatedBytes, 0, deflatedBytes.Length);
            return output.ToArray();
        }

        public static VbfMetadata Decompress(byte[] bytes)
        {
            var length = BitConverter.ToInt32(bytes, 0);
            var hash = new byte[16];
            Array.Copy(bytes, 4, hash, 0, 16);
            var input = new MemoryStream();
            var inflater = new ZOutputStream(input);
            inflater.Write(bytes, 20, length);
            inflater.finish();

            var json = Encoding.UTF8.GetString(input.ToArray());
            var metadata = JsonConvert.DeserializeObject<VbfMetadata>(json);
            return metadata;
        }
    }

    public class VbfMetadataEntry
    {
        public string Filename { get; set; }
        public ulong StartOffset { get; set; }
        public int OriginalSize { get; set; }
        public List<ushort> BlockList { get; set; } = new List<ushort>();
    }
}