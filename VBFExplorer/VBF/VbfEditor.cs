﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using VBFExplorer.zlib.net;

namespace VBFExplorer.VBF
{
    public class VbfEditor : IDisposable
    {
        private readonly MD5 _md5 = MD5.Create();
        private const long BytesRequired = 32L * 1024 * 1024; // 32MB

        public string Filename { get; }
        public bool HasMetadata { get; private set; }
        public string[] FileNames => _fileNames;

        private readonly FileStream _fileStream;
        private readonly BinaryReader _fileReader;

        private string[] _fileNames;
        private byte[][] _fileNameHashes;
        private ulong[] _fileNameOffsets;
        private ulong[] _startOffsets;
        private ulong[] _originalSizes;
        private uint[] _blockListStarts;
        private ushort[] _blockList;
        private VbfMetadata _metadata;

        public VbfEditor(string filePath)
        {
            Filename = filePath;

            _fileStream = File.Open(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            _fileReader = new BinaryReader(_fileStream);

            Read();
        }

        private void Read()
        {
            // Ensure the file starts with the appropriate bytes
            if ((int) _fileReader.ReadUInt32() != 1264144979)
                throw new Exception("Invalid header!");

            var headerLength = _fileReader.ReadUInt32();
            var numFiles = _fileReader.ReadUInt64();

            _fileNameHashes = new byte[numFiles][];
            _fileNameOffsets = new ulong[numFiles];

            _startOffsets = new ulong[numFiles];
            _originalSizes = new ulong[numFiles];
            _blockListStarts = new uint[numFiles];

            // Read MD5 hashes
            for (var i = 0UL; i < numFiles; i++)
                _fileNameHashes[i] = _fileReader.ReadBytes(16);

            // Read file information
            for (var i = 0UL; i < numFiles; i++)
            {
                _blockListStarts[i] = _fileReader.ReadUInt32();
                _fileReader.ReadUInt32(); // Unknown value, can be safely ignored
                _originalSizes[i] = _fileReader.ReadUInt64();
                _startOffsets[i] = _fileReader.ReadUInt64();
                _fileNameOffsets[i] = _fileReader.ReadUInt64();
            }

            // Read string table
            var stringTableLength = _fileReader.ReadUInt32();
            var stringTable = _fileReader.ReadBytes((int) stringTableLength - 4);
            _fileNames = Encoding.ASCII.GetString(stringTable).Trim('\0').Split('\0');
            if (_fileNames.Length != (int) numFiles)
                throw new Exception("String count mismatch.");

            // Calculate number of blocks in block list
            uint blockCount = 0;
            foreach (var originalSize in _originalSizes)
            {
                blockCount += (uint) (originalSize / 65536UL);
                if ((long) (originalSize % 65536UL) != 0L)
                    ++blockCount;
            }

            // Read block list
            _blockList = new ushort[blockCount];
            for (var i = 0U; i < blockCount; i++)
                _blockList[i] = _fileReader.ReadUInt16();

            // Read header into buffer
            _fileStream.Seek(0, SeekOrigin.Begin);
            var headerBytes = _fileReader.ReadBytes((int) headerLength);

            // Read hash from file (last 16 bytes)
            _fileStream.Seek(-16, SeekOrigin.End);
            var hashBytes = _fileReader.ReadBytes(16);

            var headerHash = MD5.Create().ComputeHash(headerBytes);
            if (!headerHash.SequenceEqual(hashBytes))
                throw new Exception("Unable to validate header hash.");

            if (_fileNames.Length == 0)
                return;

            HasMetadata = _fileNames[0] == ".vbfexplorer";
            if (HasMetadata)
                ReadMetadata();
        }

        private void ReadMetadata()
        {
            var mf = ExtractStream(".vbfexplorer");
            var metadata = VbfMetadata.Decompress(mf.ToArray());
            _metadata = metadata;
        }

        public void InstallMetadata()
        {
            // Build a dictionary of files and their start offsets
            var startOffsetDict = new Dictionary<string, ulong>();
            for (var i = 0; i < _fileNames.Length; i++)
            {
                if (_originalSizes[i] == 0 || (long) _startOffsets[i] > _fileStream.Length)
                    continue;
                startOffsetDict[_fileNames[i]] = _startOffsets[i];
            }

            // Order the dictionary by the offset of each file
            var orderedFiles = startOffsetDict.OrderBy(dd => dd.Value).ToDictionary(dd => dd.Key, dd => dd.Value);

            // Find the files that fill up the required amount of space
            var bytesRemaining = BytesRequired - (long) orderedFiles.First().Value;
            var currentFile = 0;
            var filesToMove = new List<int>();
            while (currentFile < _fileNames.Length && bytesRemaining > 0)
            {
                var file = orderedFiles.ElementAt(currentFile);
                var fileIndex = IndexOf(file.Key);
                var fileSize = _originalSizes[fileIndex];
                bytesRemaining -= (long) fileSize;
                filesToMove.Add(fileIndex);
                currentFile++;
            }

            // Calculate end of the last file in archive
            var lastFile = orderedFiles.Last();
            var lastFileIndex = IndexOf(lastFile.Key);
            var lastFileSize = _originalSizes[lastFileIndex];

            var destPosition = (long) (lastFile.Value + lastFileSize);
            if (destPosition < BytesRequired)
                destPosition = BytesRequired;

            // Copy data from source to destination
            Console.WriteLine($"Writing to offset {destPosition}");
            for (var f = 0; f < filesToMove.Count; f++)
            {
                var fileName = _fileNames[f];
                var fileIndex = filesToMove[f];
                var filePosition = _startOffsets[fileIndex];
                var fileSize = _originalSizes[f];
                var fileBlocks = (int) (fileSize / 65536) + (fileSize % 65536 == 0 ? 0 : 1);

                Console.WriteLine($"Moving {fileName} from offset {filePosition} to {destPosition}");
                var fileBytes = 0;
                for (var b = _blockListStarts[f]; b < _blockListStarts[f] + fileBlocks; b++)
                {
                    var blockSize = (int) _blockList[b];
                    if (blockSize == 0)
                        blockSize = 65536;
                    fileBytes += blockSize;
                    // Copy block bytes from source to dest
                    Console.WriteLine($"Copying {blockSize} bytes");
                }

                _startOffsets[fileIndex] = (ulong) destPosition;
                destPosition += fileBytes;
            }

            // Create new entry at end of file
            Array.Resize(ref _fileNames, _fileNames.Length + 1);
            Array.Resize(ref _fileNameHashes, _fileNameHashes.Length + 1);
            Array.Resize(ref _fileNameOffsets, _fileNameOffsets.Length + 1);
            Array.Resize(ref _startOffsets, _startOffsets.Length + 1);
            Array.Resize(ref _originalSizes, _originalSizes.Length + 1);
            Array.Resize(ref _blockListStarts, _blockListStarts.Length + 1);

            _fileNames[_fileNames.Length - 1] = _fileNames[0];
            _fileNameHashes[_fileNameHashes.Length - 1] = _fileNameHashes[0];
            _fileNameOffsets[_fileNameOffsets.Length - 1] = _fileNameOffsets[0];
            _startOffsets[_startOffsets.Length - 1] = _startOffsets[0];
            _originalSizes[_originalSizes.Length - 1] = _originalSizes[0];
            _blockListStarts[_blockListStarts.Length - 1] = _blockListStarts[0];
            _blockList[_blockList.Length - 1] = _blockList[0];

            _metadata = new VbfMetadata
            {
                LastModified = DateTime.Now,
                OriginalFiles = new List<VbfMetadataEntry>()
            };

            var meta = VbfMetadata.Compress(_metadata);

            _fileNames[0] = ".vbfexplorer";
            _startOffsets[0] = BytesRequired - 8 * 1024 * 1024;
            _originalSizes[0] = (ulong) meta.Length;
            _blockListStarts[0] = (uint) _blockList.Length;

            // Write new header
        }

        public int IndexOf(string filename)
        {
            return Array.IndexOf(_fileNames, filename);
        }

        public bool Contains(string filename)
        {
            return _fileNames.Contains(filename);
        }

        public ulong GetStartOffset(int fileIndex)
        {
            return _startOffsets[fileIndex];
        }

        public ulong GetOriginalSize(int fileIndex)
        {
            return _originalSizes[fileIndex];
        }

        public ushort[] GetBlockList(int fileIndex)
        {
            var originalSize = _originalSizes[fileIndex];
            var blockCount = originalSize / 65536;
            if (originalSize % 65536 != 0)
                blockCount++;

            var blockList = new ushort[blockCount];
            Array.Copy(_blockList, _blockListStarts[fileIndex], blockList, 0, (long) blockCount);
            return blockList;
        }

        public MemoryStream ExtractStream(string filename)
        {
            var outputStream = new MemoryStream();
            var result = ExtractToStream(filename, outputStream);

            if (!result)
                return null;

            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        public bool ExtractToStream(string filename, Stream outputStream)
        {
            // Find index of file in archive
            var fileIndex = _fileNames.ToList()
                .FindIndex(f => string.Compare(f, filename, StringComparison.OrdinalIgnoreCase) == 0);
            if (fileIndex == -1)
                return false;

            // Calculate number of blocks to read from archive
            var originalSize = _originalSizes[fileIndex];
            var blockCount = (int) (originalSize / 65536);
            var blockRemainder = (int) (originalSize % 65536);
            if (blockRemainder != 0)
                blockCount++;
            else
                blockRemainder = 65536;

            var startOffset = _startOffsets[fileIndex];
            var blockListStart = _blockListStarts[fileIndex];


            using (var stream = File.Open(Filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                // Seek to start location of block data in archive
                stream.Seek((long) startOffset, SeekOrigin.Begin);

                for (var i = 0; i < blockCount; i++)
                {
                    // Get block length from block list
                    var blockLength = (int) _blockList[blockListStart + i];

                    // Since the length value is stored as a ushort, a value of 0 means the block was 65536 bytes
                    if (blockLength == 0)
                        blockLength = 65536;

                    // Read compressed block into buffer
                    var compressedBytes = new byte[blockLength];
                    stream.Read(compressedBytes, 0, blockLength);

                    // All blocks except the very last one have an original length of 65536 bytes
                    var decompressedLength = i != blockCount - 1
                        ? 65536
                        : blockRemainder;

                    // Create a buffer for the decompressed data
                    byte[] decompressedBytes;

                    // If we are processing the final block, and the data is uncompressed, skip decompression
                    if (i == blockCount - 1)
                        if (blockLength == blockRemainder)
                            goto MoveUncompressedData;

                    // Compressed blocks have a length less than 65536 bytes
                    if (blockLength != 65536)
                    {
                        try
                        {
                            decompressedBytes = new byte[decompressedLength];

                            using (var defStream = new ZOutputStream(new MemoryStream(decompressedBytes)))
                                defStream.Write(compressedBytes, 0, blockLength);

                            goto WriteBuffer;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Failed to extract file.", ex);
                        }
                    }

                    MoveUncompressedData:
                    decompressedBytes = compressedBytes;

                    WriteBuffer:
                    outputStream.Write(decompressedBytes, 0, decompressedLength);
                }
            }

            return true;
        }

        public void Dispose()
        {
            _fileStream?.Close();
            _fileStream?.Dispose();
            _fileReader?.Dispose();
        }
    }
}