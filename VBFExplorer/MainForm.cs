﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VBFExplorer.VBF;

namespace VBFExplorer
{
    public partial class MainForm : Form
    {
        private bool _edit = false;
        private VbfEditor _vbf;
        private List<string> _directories;

        public MainForm()
        {
            InitializeComponent();
        }

        private void openVBFArchiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                Title = "Open VBF archive",
                Filter = "VBF archives|*.vbf|All files|*.*"
            };

            if (ofd.ShowDialog() != DialogResult.OK)
                return;

            OpenFile(ofd.FileName);
        }

        private async void OpenFile(string filename)
        {
            _edit = false;
            _vbf = null;
            _directories = null;
            directoryTree.Nodes.Clear();

            var result = await Task.Run(() =>
            {
                try
                {
                    _vbf = new VbfEditor(filename);
                    var directoryNames = _vbf.FileNames.Select(f => '/' + f.Substring(0, f.LastIndexOf('/') + 1));
                    var groupedDirectories = directoryNames.GroupBy(d => d).Select(d => d.Key).ToList();
                    _directories = groupedDirectories.OrderBy(d => d).ToList();
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        $"Unable to load archive. An exception occurred:\n\n{ex}",
                        "Failed to load archive");
                    return false;
                }
            });

            if (!result)
                return;

            if (enableEditModeToolStripMenuItem.Checked)
                DoEditMode();

            var vbfName = Path.GetFileName(filename);
            var rootNode = CreateNode("/", _directories, vbfName, 0);
            directoryTree.Nodes.Add(rootNode);
            rootNode.Expand();
            Text = $"{vbfName}{(_edit ? "" : " (read-only)")} - VBF Explorer";
        }

        private void DoEditMode()
        {
            if (!_vbf.HasMetadata)
            {
                var result = MessageBox.Show(
                    $"VBF Explorer needs to make some changes to the archive before edit mode can be enabled.\n\nDo you want to make the changes and enable edit mode now? Selecting no will open the archive in read-only mode.",
                    "VBF Explorer", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (result != DialogResult.Yes)
                    return;
                _vbf.InstallMetadata();
                _edit = true;
            }
        }

        private TreeNode CreateNode(string directory, List<string> subds, string name = null, int icon = 1)
        {
            var dir = directory.TrimEnd('/');
            var lastIndex = dir.LastIndexOf('/');
            var directoryName = name ?? (lastIndex == -1 ? dir : directory.Substring(lastIndex + 1)).Trim('/');
            var subs = GetSubdirs(directory, subds);
            var node = new TreeNode(directoryName)
            {
                ImageIndex = icon,
                SelectedImageIndex = icon,
                Tag = directory
            };

            if (subs.Count != 0)
                node.Nodes.Add(new TreeNode());

            return node;
        }

        private async Task ExpandNode(TreeNode node)
        {
            if (node.ImageIndex == 1)
                node.SelectedImageIndex = node.ImageIndex = 2;

            var nodeDir = (string) node.Tag;
            var subFiles = _directories.Where(d => d.StartsWith(nodeDir)).ToList();
            var subDirs = await Task.Run(() => GetSubdirs(nodeDir, subFiles));
            node.Nodes.Clear();
            node.Nodes.AddRange(subDirs.Select(d => CreateNode(d, subFiles)).ToArray());
        }

        private List<string> GetSubdirs(string directory, List<string> subs)
        {
            var dirs = subs.Where(d => d.StartsWith(directory)).ToList();
            var subdirs = new List<string>();
            foreach (var dir in dirs)
            {
                var trimParentDir = dir.Substring(directory.Length);
                var nextDirIndex = trimParentDir.IndexOf('/');
                if (nextDirIndex == -1)
                    continue;
                var subdir = directory + trimParentDir.Substring(0, nextDirIndex) + '/';
                if (!subdirs.Contains(subdir))
                    subdirs.Add(subdir);
            }
            return subdirs;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void directoryTree_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            e.Node.Nodes.Clear();
            e.Node.Nodes.Add(new TreeNode("Loading..", 3, 3));
        }

        private async void directoryTree_AfterExpand(object sender, TreeViewEventArgs e)
        {
            await ExpandNode(e.Node);
        }

        private void directoryTree_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Node.ImageIndex == 2)
                e.Node.SelectedImageIndex = e.Node.ImageIndex = 1;
        }

        private void directoryTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                var directory = e.Node.Tag.ToString().TrimStart('/');
                var files = _vbf.FileNames.Where(f => f.StartsWith(directory) && f.IndexOf('/', directory.Length) == -1)
                    .ToList();
                PopulateFiles(files);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void PopulateFiles(List<string> files)
        {
            fileView.Items.Clear();
            foreach (var file in files)
            {
                var filenameStart = file.LastIndexOf('/') + 1;
                var filename = filenameStart == -1 ? file : file.Substring(filenameStart);
                var item = new ListViewItem
                {
                    ImageIndex = GetIconIndex(file),
                    Text = filename,
                    Tag = file
                };
                fileView.Items.Add(item);
            }
        }

        private int GetIconIndex(string filename)
        {
            switch (Path.GetExtension(filename))
            {
                default:
                    return 0;
                case ".bin":
                    return 1;
                case ".fsb":
                    return 2;
            }
        }

        private void fileView_MouseDown(object sender, MouseEventArgs e)
        {
        }

        private void fileView_MouseCaptureChanged(object sender, EventArgs e)
        {
            if (!fileView.Capture)
                return;
        }

        private void fileView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            var selectedItems = fileView.SelectedItems;
            if (selectedItems.Count == 0)
                return;

            var outPath = Path.GetTempFileName();
            File.Delete(outPath);
            if (!Directory.Exists(outPath))
                Directory.CreateDirectory(outPath);

            var files = new List<string>();
            foreach (ListViewItem item in selectedItems)
            {
                var filePath = (string) item.Tag;
                var fileName = filePath.Substring(filePath.LastIndexOf('/') + 1);
                var outFile = Path.Combine(outPath, fileName);
                using (var fs = File.Create(outFile))
                    _vbf.ExtractToStream(filePath, fs);
                files.Add(outFile);
            }

            var data = new DataObject(DataFormats.FileDrop, files.ToArray());
            data.SetData(DataFormats.Text, files[0]);

            DoDragDrop(data, DragDropEffects.Move);
        }

        private void enableEditModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            enableEditModeToolStripMenuItem.Checked = !enableEditModeToolStripMenuItem.Checked;
            if (_vbf != null)
                OpenFile(_vbf.Filename);
        }
    }
}