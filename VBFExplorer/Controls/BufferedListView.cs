﻿using System.Windows.Forms;

namespace VBFExplorer.Controls
{
    public class BufferedListView : ListView
    {
        protected override bool DoubleBuffered { get; set; } = true;
    }
}