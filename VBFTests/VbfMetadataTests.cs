﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using VBFExplorer.VBF;

namespace VBFTests
{
    [TestClass]
    public class VbfMetadataTests
    {
        [TestMethod]
        public void TestCompressDecompress()
        {
            var random = new Random();
            var metadata = new VbfMetadata();
            var numFiles = random.Next(3, 10);
            var offset = 0UL;
            for (var i = 0; i < numFiles; i++)
            {
                var size = random.Next(64 * 1024, 256 * 1024);
                var blocks = size / 65536 + (size % 65536 == 0 ? 0 : 1);
                var name = Regex.Replace(Guid.NewGuid().ToString(), "[{}-]", "");

                var file = new VbfMetadataEntry
                {
                    Filename = name,
                    OriginalSize = size,
                    StartOffset = offset
                };

                offset += (ulong) size;

                for (var b = 0; b < blocks; b++)
                    file.BlockList.Add((ushort) random.Next(0, 65536));

                metadata.OriginalFiles.Add(file);
            }

            var inputJson = JsonConvert.SerializeObject(metadata);
            var compressedBytes = VbfMetadata.Compress(metadata);
            var decompressedMetadata = VbfMetadata.Decompress(compressedBytes);
            var decompressedJson = JsonConvert.SerializeObject(decompressedMetadata);
            Debug.Assert(inputJson.SequenceEqual(decompressedJson));
        }
    }
}